{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Chess.RankSpec (spec) where

import           Chess.Rank
import           RIO
import           RIO.Vector.Boxed  (fromList)
import           Test.Hspec
import           Util.BoundedIndex (BoundedIndex (..))

spec :: Spec
spec = parallel $ do
  describe "numRanks" $
    it "numRanks should be 8" $
      numRanks `shouldBe` 8

  describe "allRanks" $
    it "allRanks should be [1, 2, 3, 4, 5, 6, 7, 8]" $
      allRanks `shouldBe` fromList[First, Second, Third, Fourth, Fifth, Sixth, Seventh, Eighth]

  describe "fromIndex" $ do
    it "fromIndex 0 should be First" $
      let rank = First in
        (fromIndex 0 :: Rank) `shouldBe` rank
    it "fromIndex 1 should be Second" $
      let rank = Second in
        (fromIndex 1 :: Rank) `shouldBe` rank
    it "fromIndex 2 should be Third" $
      let rank = Third in
        (fromIndex 2 :: Rank) `shouldBe` rank
    it "fromIndex 3 should be Fourth" $
      let rank = Fourth in
        (fromIndex 3 :: Rank) `shouldBe` rank
    it "fromIndex 4 should be Fifth" $
      let rank = Fifth in
        (fromIndex 4 :: Rank) `shouldBe` rank
    it "fromIndex 5 should be Sixth" $
      let rank = Sixth in
        (fromIndex 5 :: Rank) `shouldBe` rank
    it "fromIndex 6 should be Seventh" $
      let rank = Seventh in
        (fromIndex 6 :: Rank) `shouldBe` rank
    it "fromIndex 7 should be Eighth" $
      let rank = Eighth in
        (fromIndex 7 :: Rank) `shouldBe` rank

  describe "toIndex" $ do
    it "toIndex First should be 0" $
        toIndex First `shouldBe` 0
    it "toIndex Second should be 1" $
        toIndex Second `shouldBe` 1
    it "toIndex Third should be 2" $
        toIndex Third `shouldBe` 2
    it "toIndex Fourth should be 3" $
        toIndex Fourth `shouldBe` 3
    it "toIndex Fifth should be 4" $
        toIndex Fifth `shouldBe` 4
    it "toIndex Sixth should be 5" $
        toIndex Sixth `shouldBe` 5
    it "toIndex Seventh should be 6" $
        toIndex Seventh `shouldBe` 6
    it "toIndex Eighth should be 7" $
        toIndex Eighth `shouldBe` 7

  describe "down" $ do
    it "down First should be Eighth" $
      down First `shouldBe` Eighth
    it "down Second should be First" $
      down Second `shouldBe` First
    it "down Third should be Second" $
      down Third `shouldBe` Second
    it "down Fourth should be Third" $
      down Fourth `shouldBe` Third
    it "down Fifth should be Fourth" $
      down Fifth `shouldBe` Fourth
    it "down Sixth should be Fifth" $
      down Sixth `shouldBe` Fifth
    it "down Seventh should be Sixth" $
      down Seventh `shouldBe` Sixth
    it "down Eighth should be Seventh" $
      down Eighth `shouldBe` Seventh

  describe "up" $ do
    it "up First should be Second" $
      up First `shouldBe` Second
    it "up Second should be Third" $
      up Second `shouldBe` Third
    it "up Third should be Fourth" $
      up Third `shouldBe` Fourth
    it "up Fourth should be Fifth" $
      up Fourth `shouldBe` Fifth
    it "up Fifth should be Sixth" $
      up Fifth `shouldBe` Sixth
    it "up Sixth should be Seventh" $
      up Sixth `shouldBe` Seventh
    it "up Seventh should be Eighth" $
      up Seventh `shouldBe` Eighth
    it "up Eighth should be First" $
      up Eighth `shouldBe` First

  -- describe "fromText" $ do
  --   it "fromText \"1\" should be First" $
  --     (fromText "1" :: Rank) `shouldBe` Right First
  --   it "fromText \"2\" should be Second" $
  --     (fromText "2" :: Rank) `shouldBe` Right Second
  --   it "fromText \"3\" should be Third" $
  --     (fromText "3" :: Rank) `shouldBe` Right Third
  --   it "fromText \"4\" should be Fourth" $
  --     (fromText "4" :: Rank) `shouldBe` Right Fourth
  --   it "fromText \"5\" should be Fifth" $
  --     (fromText "5" :: Rank) `shouldBe` Right Fifth
  --   it "fromText \"6\" should be Sixth" $
  --     (fromText "6" :: Rank) `shouldBe` Right Sixth
  --   it "fromText \"7\" should be Seventh" $
  --     (fromText "7" :: Rank) `shouldBe` Right Seventh
  --   it "fromText \"8\" should be Eighth" $
  --     (fromText "8" :: Rank) `shouldBe` Right Eighth
  --   it "fromText \"9\" should be Nothing" $
  --     (fromText "9" :: Rank) `shouldBe` Left InvalidRank

  describe "show" $ do
    it "show First should be 1" $
      show First `shouldBe` "1"
    it "show Second should be 2" $
      show Second `shouldBe` "2"
    it "show Third should be 3" $
      show Third `shouldBe` "3"
    it "show Fourth should be 4" $
      show Fourth `shouldBe` "4"
    it "show Fifth should be 5" $
      show Fifth `shouldBe` "5"
    it "show Sixth should be 6" $
      show Sixth `shouldBe` "6"
    it "show Seventh should be 7" $
      show Seventh `shouldBe` "7"
    it "show Eighth should be 8" $
      show Eighth `shouldBe` "8"
