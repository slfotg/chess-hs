{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Chess.File
  ( File (..)
  , numFiles
  , allFiles
  , left
  , right
  ) where

import           Data.Ix                  (Ix)
import           RIO                      (Bounded, Either (..), Enum (..), Eq,
                                           Int, Integral (mod), Maybe (Just),
                                           Num ((+), (-)), Ord, Show (..),
                                           Vector)
import           RIO.Text                 (uncons)
import           RIO.Vector.Boxed         (fromList)
import           RIO.Vector.Boxed.Partial ((!))
import           Util.BoundedIndex        (BoundedIndex (..))
import           Util.Error               (Error (InvalidFile))
import           Util.FromText            (FromChar (fromChar),
                                           FromText (fromText))

-- | Describe a file (column) on a chess board
data File = A | B | C | D | E | F | G | H
  deriving (Bounded, Eq, Enum, Ix, Ord)

instance BoundedIndex File where
  fromIndex i = allFiles ! (i `mod` numFiles)

-- | How many files are there?
numFiles :: Int
numFiles = 8

-- | Enumerate all files
allFiles :: Vector File
allFiles = fromList [A, B, C, D, E, F, G, H]

-- | Go one file to the left.  If impossible, wrap around.
left :: File -> File
left file = fromIndex (toIndex file - 1)

-- | Go one file to the right.  If impossible, wrap around.
right :: File -> File
right file = fromIndex (toIndex file + 1)

-- | Convert a `String` into a `File`
instance FromText File where
  fromText text =
    case uncons text of
      Just (c, "") -> fromChar c
      _            -> Left InvalidFile

instance FromChar File where
  fromChar 'a' = Right A
  fromChar 'b' = Right B
  fromChar 'c' = Right C
  fromChar 'd' = Right D
  fromChar 'e' = Right E
  fromChar 'f' = Right F
  fromChar 'g' = Right G
  fromChar 'h' = Right H
  fromChar _   = Left InvalidFile

instance Show File where
  show A = "a"
  show B = "b"
  show C = "c"
  show D = "d"
  show E = "e"
  show F = "f"
  show G = "g"
  show H = "h"
