{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TupleSections     #-}
module Chess.Board
  ( Board
  , BoardStatus (..)
  , new
  , status
  , colorCombined
  , kingSquare
  , combined
  , fromStr
  , defaultBoard
  , pieces
  , castleRights
  , sideToMove
  , myCastleRights
  , theirCastleRights
  , nullMove
  , isSane, pieceOn, colorOn, enPassant) where

import           Chess.BitBoard     (BitBoard)
import qualified Chess.BitBoard     as BitBoard
import           Chess.CastleRights (CastleRights (..))
import           Chess.Color        (Color (..), other)
import           Chess.Piece        (Piece (..))
import           Chess.Square       (Square)
import           Data.Array         (Array, array, (!))
import           Data.Bits          (popCount, xor, (.&.), (.|.))
import           Data.Ix            (Ix, range)
import           RIO                (Bool (..), Bounded (..), Eq,
                                     Maybe (Just, Nothing), Ord, Show, String,
                                     Word64, all, and, error, filter, foldr,
                                     map, otherwise, uncurry, undefined, ($),
                                     (.), (/=), (<$>), (<*>), (==))
import           Util.BoundedIndex  (iter)

data Board = Board
  { _pieces        :: !(Array Piece BitBoard)
  , _colorCombined :: !(Array Color BitBoard)
  , _combined      :: !BitBoard
  , _sideToMove    :: !Color
  , _castleRights  :: !(Array Color CastleRights)
  , _pinned        :: !BitBoard
  , _checkers      :: !BitBoard
  , _hash          :: !Word64
  , _enPassant     :: !(Maybe Square)
  }
  deriving (Eq, Show)

data BoardStatus = Ongoing | Stalemate | Checkmate
  deriving (Eq, Ord, Show)

defaultBoard :: Board
defaultBoard =
  case fromStr "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" of
    Just b -> b
    _      -> error "Valid Position"

_defaultArray :: (Bounded i, Ix i) => a -> Array i a
_defaultArray a = let bounds = (minBound, maxBound) in
  array bounds $ map (, a) $ range bounds

new :: Board
new = Board
  { _pieces        = _defaultArray BitBoard.empty
  , _colorCombined = _defaultArray BitBoard.empty
  , _combined      = BitBoard.empty
  , _sideToMove    = White
  , _castleRights  = _defaultArray NoRights
  , _pinned        = BitBoard.empty
  , _checkers      = BitBoard.empty
  , _hash          = 0
  , _enPassant     = Nothing
  }

status :: Board -> BoardStatus
status _ = undefined

combined :: Board -> BitBoard
combined = _combined

colorCombined :: Board -> Color -> BitBoard
colorCombined = (!) . _colorCombined

kingSquare :: Board -> Color -> Square
kingSquare board color =
  BitBoard.toSquare
    $ (_pieces board ! King)
      .&. colorCombined board color

pieces :: Board -> Piece -> BitBoard
pieces = (!) . _pieces

castleRights :: Board -> Color -> CastleRights
castleRights = (!) . _castleRights

sideToMove :: Board -> Color
sideToMove = _sideToMove

myCastleRights :: Board -> CastleRights
myCastleRights board = castleRights board $ sideToMove board

theirCastleRights :: Board -> CastleRights
theirCastleRights board = castleRights board $ other $ sideToMove board

-- xor :: Board -> Piece -> BitBoard -> Color -> Board
-- xor = undefined

nullMove :: Board -> Maybe Board
nullMove board =
  if _checkers board == BitBoard.empty
    then Just $ updatePinInfo
              $ board
                { _sideToMove = other $ sideToMove board
                , _enPassant = Nothing
                }
    else Nothing

{- | Does this board "make sense"?
Do all the pieces make sense, do the bitboards combine correctly, etc?
This is for sanity checking.
-}
isSane :: Board -> Bool
isSane board =
  and [ checkOverLappingPieces
      , checkOverLappingColors
      , checkCombinedPieces
      , checkSingleKing White
      , checkSingleKing Black
      , checkEnPassant
      , checkOpponentChecks
      , checkCastleRights White
      , checkCastleRights Black
      , checkKingsNotTouching
      ]
  where
    -- make sure there is no square with multiple pieces on it
    checkOverLappingPieces :: Bool
    checkOverLappingPieces = let allPieces = iter :: [Piece]
      in
        all (\(x, y) -> pieces board x .&. pieces board y == BitBoard.empty)
        $ filter (uncurry (/=)) $ (,) <$> allPieces <*> allPieces

    -- make sure the colors don't overlap, either
    checkOverLappingColors :: Bool
    checkOverLappingColors =
      colorCombined board White .&. colorCombined board Black == BitBoard.empty

    -- grab all the pieces by OR'ing together each piece() BitBoard
    -- make sure that's equal to the combined bitboard
    checkCombinedPieces :: Bool
    checkCombinedPieces =
      foldr (.|.) BitBoard.empty (_pieces board) == _combined board

    -- make sure there is exactly one king of a certain color
    checkSingleKing :: Color -> Bool
    checkSingleKing color =
      popCount (pieces board King .&. colorCombined board color) == 1

    -- make sure the en_passant square has a pawn on it of the right color
    checkEnPassant :: Bool
    checkEnPassant = case _enPassant board of
      Just square -> pieces board Pawn
                  .&. colorCombined board (other $ sideToMove board)
                  .&. BitBoard.fromSquare square
                  /= BitBoard.empty
      Nothing     -> True

    -- make sure my opponent is not currently in check (because that would be illegal)
    checkOpponentChecks :: Bool
    checkOpponentChecks =
      _checkers (updatePinInfo board{ _sideToMove = other $ _sideToMove board })
      == BitBoard.empty

    checkCastleRights :: Color -> Bool
    checkCastleRights White = undefined
    checkCastleRights Black = undefined

    checkKingsNotTouching :: Bool
    checkKingsNotTouching = undefined

pieceOn :: Board -> Square -> Maybe Piece
pieceOn board square =
  let opp = BitBoard.fromSquare square
      ps = pieces board
    in
      if combined board .&. opp == BitBoard.empty
        then Nothing
        else
          if (ps Pawn `xor` ps Knight `xor` ps Bishop) .&. opp /= BitBoard.empty
            then
              if ps Pawn .&. opp /= BitBoard.empty
                then Just Pawn
                else
                  if ps Knight .&. opp /= BitBoard.empty
                    then Just Knight
                    else Just Bishop
            else
              if ps Rook .&. opp /= BitBoard.empty
                then Just Rook
                else
                  if ps Queen .&. opp /= BitBoard.empty
                    then Just Queen
                    else Just King

colorOn :: Board -> Square -> Maybe Color
colorOn board square
  | colorCombined board White .&. BitBoard.fromSquare square /= BitBoard.empty = Just White
  | colorCombined board Black .&. BitBoard.fromSquare square /= BitBoard.empty = Just Black
  | otherwise = Nothing

enPassant :: Board -> Maybe Square
enPassant = _enPassant

updatePinInfo :: Board -> Board
updatePinInfo = undefined

fromStr :: String -> Maybe Board
fromStr _ = undefined
