{-# LANGUAGE NoImplicitPrelude #-}
module Chess.CastleRights
  ( CastleRights (..)
  , numCastleRights
  , castlesPerSquare
  , hasKingSide
  , hasQueenSide
  , squareToCastleRights
  , kingsideCastleRights
  , toString) where

import           Chess.BitBoard     (BitBoard)
import           Chess.Color        (Color (..))
import           Chess.Square       (Square)
import           Data.Bits          ((.&.))
import           Data.Char          (toUpper)
import           Data.Ix            (Ix)
import           RIO                (Bool, Bounded, Enum, Eq ((==)), Int, Ord,
                                     Show, String, Vector, fst, id, map, snd,
                                     ($))
import           RIO.Vector.Boxed   (fromList)
import           RIO.Vector.Partial ((!))
import           Util.BoundedIndex  (BoundedIndex (..))

-- |  What castle rights does a particular player have?
data CastleRights
  = NoRights  -- ^ Cannot castle
  | KingSide  -- ^ Can castle on king side only
  | QueenSide -- ^ Can castle on queen side only
  | Both      -- ^ Can castle on both king and queen side
    deriving (Bounded, Eq, Enum, Ix, Ord, Show)

instance BoundedIndex CastleRights

numCastleRights :: Int
numCastleRights = 4

castlesPerSquare :: (Vector CastleRights, Vector CastleRights)
castlesPerSquare =
  ( fromList
    $ map fromIndex
      [ 2, 0, 0, 0, 3, 0, 0, 1
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      ]
  , fromList
    $ map fromIndex
      [ 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 0, 0, 0, 0, 0, 0, 0, 0
      , 2, 0, 0, 0, 3, 0, 0, 1
      ]
  )

{- | Can I castle kingside?

>>> hasKingSide NoRights
False

>>> hasKingSide KingSide
True

>>> hasKingSide QueenSide
False

>>> hasKingSide Both
True
-}
hasKingSide :: CastleRights -> Bool
hasKingSide r = 1 == 1 .&. toIndex r

{- | Can I castle queenside?

>>> hasQueenSide NoRights
False

>>> hasQueenSide KingSide
False

>>> hasQueenSide QueenSide
True

>>> hasQueenSide Both
True
-}
hasQueenSide :: CastleRights -> Bool
hasQueenSide r = 2 == 2 .&. toIndex r

kingsideCastleRights :: Vector BitBoard
kingsideCastleRights = fromList []

squareToCastleRights :: Color
                     -> Square
                     -> CastleRights
squareToCastleRights White square = fst castlesPerSquare ! toIndex square
squareToCastleRights Black square = snd castlesPerSquare ! toIndex square

toString :: CastleRights
         -> Color
         -> String
toString cr c =
  fromColor $ case cr of
    NoRights  -> ""
    KingSide  -> "k"
    QueenSide -> "q"
    Both      -> "kq"
  where
    fromColor :: String
              -> String
    fromColor =
      case c of
        White -> map toUpper
        Black -> id
