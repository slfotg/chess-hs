{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Chess.BoardBuilder
  ( BoardBuilder (..)
  , newBoardBuilder
  , setup
  , getSideToMove
  , getCastleRights
  , getEnPassant
  , setSideToMove
  , setCastleRights
  , setPiece
  , clearSquare
  , setEnPassant
  , fromBoard, defaultBoardBuilder) where

import           Chess.Board        (Board)
import           Chess.CastleRights (CastleRights (..))
import qualified Chess.CastleRights as CastleRights
import           Chess.Color        (Color (..))
import qualified Chess.Color        as Color
import           Chess.File         (File (..), allFiles)
import           Chess.Piece        (Piece (..))
import qualified Chess.Piece        as Piece
import           Chess.Rank         (Rank (..), allRanks)
import           Chess.Square       (Square (..))
import qualified Chess.Square       as Square
import           Data.Array         (Array, array, listArray, (!), (//))
import           RIO                (Bounded (..), Either (..),
                                     Foldable (toList), Maybe (..), Show,
                                     String, Traversable (sequence), concat,
                                     concatMap, drop, elem, error, join, length,
                                     map, maybe, replicate, reverse, show,
                                     uncurry, undefined, unwords, zipWithM, ($),
                                     (++), (.), (/=), (<$>), (<*>), (==), (||))
import           RIO.Char           (isDigit)
import           RIO.List           (group, intercalate)
import           RIO.List.Partial   (head)
import           RIO.Partial        (read)
import           RIO.Text           (unpack, words)
import           RIO.Text.Partial   (splitOn)
import           Util.Error         (Error (..), Result)
import           Util.FromText      (FromText (fromText))

type ColoredPiece = (Piece, Color)

data BoardBuilder = BoardBuilder
  { _pieces       :: !(Array Square (Maybe ColoredPiece))
  , _sideToMove   :: !Color
  , _castleRights :: !(Array Color CastleRights)
  , _enPassant    :: !(Maybe File)
  }

emptyPieces :: Array Square (Maybe ColoredPiece)
emptyPieces = listArray (minBound, maxBound) (replicate 64 Nothing)

newBoardBuilder :: BoardBuilder
newBoardBuilder = BoardBuilder
  { _pieces       = emptyPieces
  , _sideToMove   = White
  , _castleRights = array (White, Black) [(White, NoRights), (Black, NoRights)]
  , _enPassant    = Nothing
  }

setup :: [(Square, Piece, Color)]
      -> Color
      -> CastleRights
      -> CastleRights
      -> Maybe File
      -> BoardBuilder
setup pieces'
      sideToMove'
      whiteCastleRights'
      blackCastleRights'
      enPassant'
      = BoardBuilder
        { _pieces = emptyPieces // map (\(s, p, c) -> (s, Just (p, c))) pieces'
        , _sideToMove = sideToMove'
        , _castleRights = array (White, Black) [(White, whiteCastleRights'), (Black, blackCastleRights')]
        , _enPassant = enPassant'
        }

getSideToMove :: BoardBuilder -> Color
getSideToMove = _sideToMove

setSideToMove :: BoardBuilder -> Color -> BoardBuilder
setSideToMove bb c = bb {_sideToMove = c}

getCastleRights :: BoardBuilder -> Array Color CastleRights
getCastleRights = _castleRights

setCastleRights :: BoardBuilder -> Color -> CastleRights -> BoardBuilder
setCastleRights bb c cr
  = bb { _castleRights = _castleRights bb // [(c, cr)] }

{-|
>>> getEnPassant $ newBoardBuilder {_enPassant = Just D}
Just d5

>>> getEnPassant $ newBoardBuilder {_sideToMove = Black, _enPassant = Just D}
Just d4
-}
getEnPassant :: BoardBuilder -> Maybe Square
getEnPassant bb
  = Square.makeSquare (Color.toFourthRank (Color.other $ getSideToMove bb))
  <$> _enPassant bb

setPiece :: BoardBuilder
         -> Square
         -> Piece
         -> Color
         -> BoardBuilder
setPiece bb s p c = bb { _pieces = _pieces bb // [(s, Just (p, c))] }

clearSquare :: BoardBuilder
            -> Square
            -> BoardBuilder
clearSquare bb s = bb { _pieces = _pieces bb // [(s, Nothing)] }

setEnPassant :: BoardBuilder
             -> Maybe File
             -> BoardBuilder
setEnPassant bb ep = bb { _enPassant = ep }

{- |

>>> defaultBoardBuilder
rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq -
-}
defaultBoardBuilder :: BoardBuilder
defaultBoardBuilder = case fromText "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1" of
  Right bb -> bb
  Left e   -> error $ show e -- this should never happen

instance Show BoardBuilder where
  show bb = unwords
              [ showPieces
              , showSide
              , showCastles
              , showEnpassant
              ]
    where
      showPieces :: String
      showPieces = intercalate "/" . map showRank . reverse $ toList allRanks

      showRank :: Rank -> String
      showRank rank =
        concatMap
          showGroup
          $ group
            $ map
              ((_pieces bb !) . Square.makeSquare rank)
              $ toList allFiles

      showGroup :: [Maybe ColoredPiece] -> String
      showGroup pieces =
        case head pieces of
          Just cp ->
            concat
              $ replicate
                (length pieces)
                $ uncurry
                  Piece.toString
                  cp
          Nothing -> show $ length pieces

      showSide :: String
      showSide =
        case getSideToMove bb of
          White -> "w"
          Black -> "b"

      showCastles :: String
      showCastles =
        CastleRights.toString (getCastleRights bb ! White) White
        ++ CastleRights.toString (getCastleRights bb ! Black) Black

      showEnpassant :: String
      showEnpassant = maybe "-" show (getEnPassant bb)

instance FromText BoardBuilder where
  fromText str
    = let tokens = words str
      in
        case tokens of
          (pieces:side:castles:ep:_) ->
            BoardBuilder
              <$> fromPieces
              <*> fromSide
              <*> fromCastles
              <*> fromEnPassant
              where
                fromPieces :: Result (Array Square (Maybe ColoredPiece))
                fromPieces =
                  let rankStrings = map unpack $ splitOn "/" pieces
                    in
                      if length rankStrings /= 8
                        then Left $ InvalidFen str
                        else
                          (emptyPieces //)
                            . join
                            <$> zipWithM fromRankStr (reverse $ toList allRanks) rankStrings

                fromRankStr :: Rank -> String -> Result [(Square, Maybe ColoredPiece)]
                fromRankStr rank rankStr = sequence $ fromRankStr' (toList allFiles) rankStr
                  where
                    fromRankStr' :: [File] -> String -> [Result (Square, Maybe ColoredPiece)]
                    fromRankStr' [] []               = []
                    fromRankStr' [] _                = [Left $ InvalidFen str]
                    fromRankStr' _ []                = [Left $ InvalidFen str]
                    fromRankStr' files@(f:fs) (c:cs) =
                      if isDigit c
                        then fromRankStr' (drop (read [c]) files) cs
                        else case c of
                          'p' -> Right (Square.makeSquare rank f, Just (Pawn  , Black)) : fromRankStr' fs cs
                          'n' -> Right (Square.makeSquare rank f, Just (Knight, Black)) : fromRankStr' fs cs
                          'b' -> Right (Square.makeSquare rank f, Just (Bishop, Black)) : fromRankStr' fs cs
                          'r' -> Right (Square.makeSquare rank f, Just (Rook  , Black)) : fromRankStr' fs cs
                          'q' -> Right (Square.makeSquare rank f, Just (Queen , Black)) : fromRankStr' fs cs
                          'k' -> Right (Square.makeSquare rank f, Just (King  , Black)) : fromRankStr' fs cs
                          'P' -> Right (Square.makeSquare rank f, Just (Pawn  , White)) : fromRankStr' fs cs
                          'N' -> Right (Square.makeSquare rank f, Just (Knight, White)) : fromRankStr' fs cs
                          'B' -> Right (Square.makeSquare rank f, Just (Bishop, White)) : fromRankStr' fs cs
                          'R' -> Right (Square.makeSquare rank f, Just (Rook  , White)) : fromRankStr' fs cs
                          'Q' -> Right (Square.makeSquare rank f, Just (Queen , White)) : fromRankStr' fs cs
                          'K' -> Right (Square.makeSquare rank f, Just (King  , White)) : fromRankStr' fs cs
                          _ -> [Left $ InvalidFen str]

                fromSide :: Result Color
                fromSide = case side of
                  "w" -> Right White
                  "W" -> Right White
                  "b" -> Right Black
                  "B" -> Right Black
                  _   -> Left $ InvalidFen str

                fromCastles :: Result (Array Color CastleRights)
                fromCastles
                  = Right
                  $ array (White, Black)
                  [ ( White
                    , if 'K' `elem` unpack castles
                        then
                          if 'Q' `elem` unpack castles
                            then Both
                            else KingSide
                        else
                          if 'Q' `elem` unpack castles
                            then QueenSide
                            else NoRights
                    )
                  , (Black
                    , if 'k' `elem` unpack castles
                        then
                          if 'q' `elem` unpack castles
                            then Both
                            else KingSide
                        else
                          if 'q' `elem` unpack castles
                            then QueenSide
                            else NoRights
                    )
                  ]

                fromEnPassant :: Result (Maybe File)
                fromEnPassant = case fromText ep :: Result Square of
                  Right square -> if Square.getRank square == Third || Square.getRank square == Sixth
                                    then Right $ Just $ Square.getFile square
                                    else Left $ InvalidEnPassant ep
                  _            -> if ep == "-"
                                    then Right Nothing
                                    else Left $ InvalidEnPassant ep
          _ -> Left $ InvalidFen str

fromBoard :: Board -> BoardBuilder
fromBoard = undefined

