{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Chess.Rank
  ( Rank (..)
  , numRanks
  , allRanks
  , down
  , up
  ) where

import           Data.Array               (Ix)
import           RIO                      (Bounded, Either (..), Enum (..), Eq,
                                           Int, Integral (mod), Maybe (..),
                                           Num ((+), (-)), Ord, Show (..),
                                           Vector)
import           RIO.Text                 (uncons)
import           RIO.Vector.Boxed         (fromList)
import           RIO.Vector.Boxed.Partial ((!))
import           Util.BoundedIndex        (BoundedIndex (..))
import           Util.Error               (Error (..))
import           Util.FromText            (FromChar (fromChar),
                                           FromText (fromText))

-- | Describe a rank (row) on a chess board
data Rank = First
          | Second
          | Third
          | Fourth
          | Fifth
          | Sixth
          | Seventh
          | Eighth
  deriving (Eq, Enum, Ord, Bounded, Ix)

instance BoundedIndex Rank where
  fromIndex i = allRanks ! (i `mod` numRanks)

-- | How many ranks are there?
numRanks :: Int
numRanks = 8

-- | Enumerate all ranks
allRanks :: Vector Rank
allRanks =
  fromList
    [ First
    , Second
    , Third
    , Fourth
    , Fifth
    , Sixth
    , Seventh
    , Eighth
    ]

-- | Go one rank down. If impossible, wrap around
down :: Rank -> Rank
down r = fromIndex (toIndex r - 1)

-- | Go one rank up. If impossible, wrap around
up :: Rank -> Rank
up r = fromIndex (toIndex r + 1)

instance FromText Rank where
  fromText text =
    case uncons text of
      Just (c, "") -> fromChar c
      _            -> Left InvalidRank

instance FromChar Rank where
  fromChar '1' = Right First
  fromChar '2' = Right Second
  fromChar '3' = Right Third
  fromChar '4' = Right Fourth
  fromChar '5' = Right Fifth
  fromChar '6' = Right Sixth
  fromChar '7' = Right Seventh
  fromChar '8' = Right Eighth
  fromChar _   = Left InvalidRank

instance Show Rank where
  show First   = "1"
  show Second  = "2"
  show Third   = "3"
  show Fourth  = "4"
  show Fifth   = "5"
  show Sixth   = "6"
  show Seventh = "7"
  show Eighth  = "8"
