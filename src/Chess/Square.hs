{-# LANGUAGE NoImplicitPrelude #-}
module Chess.Square
  ( Square (..)
  , numSquares
  , defaultSquare
  , fromIndex
  , toIndex
  , makeSquare
  , getRank
  , getFile
  , up
  , down
  , left
  , right
  , forward
  , backward
  , uup
  , udown
  , uleft
  , uright
  , uforward
  , ubackward
  ) where

import           Chess.Color              (Color (..))
import           Chess.File               (File (..))
import qualified Chess.File               as File
import           Chess.Rank               (Rank (..))
import qualified Chess.Rank               as Rank
import           Data.Bits                (Bits ((.&.)))
import           Data.Ix                  (Ix)
import           RIO                      (Applicative ((<*>)),
                                           Bifunctor (first), Bounded,
                                           Either (Left), Enum (..), Eq, Int,
                                           Integral (div, mod), Maybe (..),
                                           Num ((*), (+)), Ord, Show (..),
                                           Vector, const, ($), (++), (<$>))
import qualified RIO.Text                 as Text
import           RIO.Vector.Boxed         (length)
import           RIO.Vector.Boxed.Partial ((!))
import           Util.BoundedIndex        (BoundedIndex (..))
import           Util.Error               (Error (..))
import           Util.FromText            (FromChar (fromChar),
                                           FromText (fromText))

-- | Represent a square on the chess board
data Square = A1 | B1 | C1 | D1 | E1 | F1 | G1 | H1
            | A2 | B2 | C2 | D2 | E2 | F2 | G2 | H2
            | A3 | B3 | C3 | D3 | E3 | F3 | G3 | H3
            | A4 | B4 | C4 | D4 | E4 | F4 | G4 | H4
            | A5 | B5 | C5 | D5 | E5 | F5 | G5 | H5
            | A6 | B6 | C6 | D6 | E6 | F6 | G6 | H6
            | A7 | B7 | C7 | D7 | E7 | F7 | G7 | H7
            | A8 | B8 | C8 | D8 | E8 | F8 | G8 | H8
  deriving (Bounded, Eq, Enum, Ord, Ix)

instance BoundedIndex Square where
  fromIndex i = allElements ! (i `mod` numSquares)

-- | How many squares are there?
numSquares :: Int
numSquares = length (allElements :: Vector Square)

{- | Create a square on A1

__Examples:__

>>> defaultSquare
a1
-}
defaultSquare :: Square
defaultSquare = A1

{- | Make a square given a rank and a file

__Examples:__

>>> makeSquare Fifth B
b5

>>> makeSquare Fourth G
g4

>>> makeSquare First A
a1
-}
makeSquare :: Rank -> File -> Square
makeSquare rank file = fromIndex $ toIndex rank * 8 + toIndex file

{- | Return the rank given this square.

__Examples:__

>>> getRank F3
3

>>> getRank G1
1

>>> getRank H2
2

>>> getRank B4
4
-}
getRank :: Square -> Rank
getRank square = fromIndex $ toIndex square `div` 8

{- | Return the file given this square.

__Examples:__

>>> getFile F3
f

>>> getFile G1
g

>>> getFile H2
h

>>> getFile B4
b
-}
getFile :: Square -> File
getFile square = fromIndex $ toIndex square .&. 7

{- | If there is a square above me, return that.
Otherwise, Nothing.

__Examples:__

>>> up D7
Just d8

>>> up D8
Nothing
-}
up :: Square -> Maybe Square
up square = case getRank square of
  Eighth -> Nothing
  rank   -> Just $ makeSquare (Rank.up rank) (getFile square)

{- | If there is a square below me, return that.
Otherwise, Nothing.

__Examples:__

>>> down D2
>>> down D1
Just d1
Nothing
-}
down :: Square -> Maybe Square
down square = case getRank square of
  First -> Nothing
  rank  -> Just $ makeSquare (Rank.down rank) (getFile square)

{- | If there is a square to the left of me, return that.
Otherwise, Nothing.

__Examples:__

>>> left B7
>>> left A7
Just a7
Nothing
-}
left :: Square -> Maybe Square
left square = case getFile square of
  A    -> Nothing
  file -> Just $ makeSquare (getRank square) (File.left file)

{- | If there is a square to the right of me, return that.
Otherwise, Nothing.

__Examples:__

>>> right G7
>>> right H7
Just h7
Nothing
-}
right :: Square -> Maybe Square
right square = case getFile square of
  H    -> Nothing
  file -> Just $ makeSquare (getRank square) (File.right file)

{- | If there is a square "forward", given my `Color`, go in that direction.
Otherwise, Nothing.

__Examples:__

>>> forward White D7
>>> forward White D8
>>> forward Black D2
>>> forward Black D1
Just d8
Nothing
Just d1
Nothing
-}
forward :: Color -> Square -> Maybe Square
forward White square = up square
forward Black square = down square

{- | If there is a square "backward", given my `Color`, go in that direction.  Otherwise, None.

__Examples:__

>>> backward Black D7
>>> backward Black D8
>>> backward White D2
>>> backward White D1
Just d8
Nothing
Just d1
Nothing
-}
backward :: Color -> Square -> Maybe Square
backward White square = down square
backward Black square = up square

{- | If there is a square above me, return that.
If not, wrap around to the other side.

__Examples:__

>>> uup D7
>>> uup D8
d8
d1
-}
uup :: Square -> Square
uup square = case getRank square of
  Eighth -> makeSquare First (getFile square)
  rank   -> makeSquare (Rank.up rank) (getFile square)

{- | If there is a square below me, return that.
If not, wrap around to the other side.

__Examples:__

>>> udown D2
>>> udown D1
d1
d8
-}
udown :: Square -> Square
udown square = case getRank square of
  First -> makeSquare Eighth (getFile square)
  rank  -> makeSquare (Rank.down rank) (getFile square)

{- | If there is a square to the left of me, return that.
If not, wrap around to the other side.

__Examples:__

>>> uleft B7
>>> uleft A7
a7
h7
-}
uleft :: Square -> Square
uleft square = case getFile square of
  A    -> makeSquare (getRank square) H
  file -> makeSquare (getRank square) (File.left file)

{- | If there is a square to the right of me, return that.
If not, wrap around to the other side.

__Examples:__

>>> uright G7
>>> uright H7
h7
a7
-}
uright :: Square -> Square
uright square = case getFile square of
  H    -> makeSquare (getRank square) A
  file -> makeSquare (getRank square) (File.right file)

{- | If there is a square forward, given my color, return that.
If not, wrap around to the other side.

__Examples:__

>>> uforward White D7
>>> uforward White D8
d8
d1

>>> uforward Black D2
>>> uforward Black D1
d1
d8
-}
uforward :: Color -> Square -> Square
uforward White square = uup square
uforward Black square = udown square

{- | If there is a square backward, given my color, return that.
If not, wrap around to the other side.

__Examples:__

>>> ubackward White D2
>>> ubackward White D1
d1
d8

>>> ubackward Black D7
>>> ubackward Black D8
d8
d1
-}
ubackward :: Color -> Square -> Square
ubackward White square = udown square
ubackward Black square = uup square

{- | Convert a `String` into a `Square`

__Examples:__

>>> fromText $ Text.pack "a1" :: Either Error Square
>>> fromText $ Text.pack "e6" :: Either Error Square
>>> fromText $ Text.pack "f7" :: Either Error Square
>>> fromText $ Text.pack "g8" :: Either Error Square
>>> fromText $ Text.pack "a9" :: Either Error Square
>>> fromText $ Text.pack "i7" :: Either Error Square
>>> fromText $ Text.pack "a"  :: Either Error Square
Right a1
Right e6
Right f7
Right g8
Left The string specified does not contain a valid algebraic notation square
Left The string specified does not contain a valid algebraic notation square
Left The string specified does not contain a valid algebraic notation square

-}
instance FromText Square where
  fromText str =
    first
      (const InvalidSquare)
      $ case Text.unpack str of
        [file, rank] -> makeSquare <$> fromChar rank <*> fromChar file
        _            -> Left InvalidSquare

instance Show Square where
  show s = show (getFile s) ++ show (getRank s)
