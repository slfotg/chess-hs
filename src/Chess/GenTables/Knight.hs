{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.Knight
  ( knightMoves
  ) where

import           Chess.BitBoard     (BitBoard)
import qualified Chess.BitBoard     as BitBoard
import           Chess.Square       (Square (..))
import qualified Chess.Square       as Square
import           Data.Array.Unboxed (UArray, (!))
import           RIO                (Eq ((==)), Num (abs, (-)), ($), (||))
import           Util.BoundedIndex  (makeArray, toIndex)

{-|
-}
knightMoves' :: UArray Square BitBoard
knightMoves' =
  makeArray
    (\ source ->
        BitBoard.fromFilter
          (\dest ->
            let sourceRank = toIndex $ Square.getRank source
                destRank   = toIndex $ Square.getRank dest
                sourceFile = toIndex $ Square.getFile source
                destFile   = toIndex $ Square.getFile dest
                diffs      = (abs (sourceRank - destRank), abs (sourceFile - destFile))
              in
                diffs == (1, 2) || diffs == (2, 1)
          )
    )

{-|

>>> BitBoard.BBShow $ knightMoves Square.D5
. . X . X . . .
. X . . . X . .
. . . . . . . .
. X . . . X . .
. . X . X . . .
. . . . . . . .
. . . . . . . .
-}
knightMoves :: Square -> BitBoard
knightMoves = (!) knightMoves'
