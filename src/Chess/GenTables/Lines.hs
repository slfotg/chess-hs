{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.Lines
  ( lines
  ) where

import           Chess.BitBoard     (BitBoard)
import qualified Chess.BitBoard     as BitBoard
import           Chess.Square       (Square)
import qualified Chess.Square       as Square
import           Data.Array.Unboxed (UArray, (!))
import           RIO                (Bool, Eq ((/=), (==)), Num (abs, (-)),
                                     uncurry, ($), (&&), (||))
import           Util.BoundedIndex  (makeArray, toIndex)

lines' :: UArray (Square, Square) BitBoard
lines' = makeArray $ uncurry createLines

createLines :: Square -> Square -> BitBoard
createLines source dest = BitBoard.fromFilter (linesFilter source dest)

linesFilter :: Square -> Square -> Square -> Bool
linesFilter source dest test =
  let sourceRank = toIndex $ Square.getRank source
      destRank   = toIndex $ Square.getRank dest
      testRank   = toIndex $ Square.getRank test
      sourceFile = toIndex $ Square.getFile source
      destFile   = toIndex $ Square.getFile dest
      testFile   = toIndex $ Square.getFile test
    in
      if abs (sourceRank - destRank) == abs (sourceFile - destFile)
        then
          abs (sourceRank - testRank) == abs (sourceFile - testFile)
            && abs (destRank - testRank) == abs (destFile - testFile)
            && source /= dest
        else
          ((sourceRank == destRank || sourceFile == destFile) && source /= dest)
          &&
            ( (sourceRank == testRank
                && destRank == testRank
              )
              ||
              (sourceFile == testFile
                && destFile == testFile
              )
            )

{-|

>>> BitBoard.BBShow $ lines (Square.A4, Square.B5)
. . . . X . . .
. . . X . . . .
. . X . . . . .
. X . . . . . .
X . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ lines (Square.A4, Square.H4)
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
X X X X X X X X
. . . . . . . .
. . . . . . . .
. . . . . . . .
-}
lines :: (Square, Square) -> BitBoard
lines = (!) lines'
