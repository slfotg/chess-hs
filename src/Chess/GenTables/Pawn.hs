{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.Pawn
  ( pawnMoves
  , pawnAttacks
  ) where

import           Chess.BitBoard     (BitBoard)
import qualified Chess.BitBoard     as BitBoard
import           Chess.Color        (Color)
import qualified Chess.Color        as Color
import           Chess.Square       (Square)
import qualified Chess.Square       as Square
import           Data.Array.Unboxed (UArray, (!))
import           Data.Bits          ((.|.))
import           RIO                (Eq ((==)), Foldable (foldr), curry, maybe,
                                     ($), (.))
import           Util.BoundedIndex  (makeArray)

{- |

>>> BitBoard.BBShow $ pawnMoves Color.White Square.E2
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . X . . .
. . . . X . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnMoves Color.Black Square.E7
. . . . . . . .
. . . . . . . .
. . . . X . . .
. . . . X . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnMoves Color.White Square.C6
. . . . . . . .
. . X . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnMoves Color.Black Square.D5
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . X . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
-}
pawnMoves :: Color -> Square -> BitBoard
pawnMoves = curry (generatePawnMoves !)

{- |

>>> BitBoard.BBShow $ pawnAttacks Color.White Square.E2
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . X . X . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnAttacks Color.Black Square.E7
. . . . . . . .
. . . . . . . .
. . . X . X . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnAttacks Color.White Square.C6
. . . . . . . .
. X . X . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnAttacks Color.Black Square.D5
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . X . X . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnAttacks Color.White Square.H5
. . . . . . . .
. . . . . . . .
. . . . . . X .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .

>>> BitBoard.BBShow $ pawnAttacks Color.Black Square.H5
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . X .
. . . . . . . .
. . . . . . . .
. . . . . . . .
-}
pawnAttacks :: Color -> Square -> BitBoard
pawnAttacks = curry (generatePawnAttacks !)

generatePawnMoves :: UArray (Color, Square) BitBoard
generatePawnMoves =
  makeArray
    ( \(c, s) ->
        if Square.getRank s == Color.toSecondRank c
          then
            foldr
              (.|.)
              BitBoard.empty
              [ BitBoard.fromSquare $ Square.uforward c s
              , BitBoard.fromSquare $ Square.uforward c $ Square.uforward c s
              ]
          else
            maybe
              BitBoard.empty
              BitBoard.fromSquare
              $ Square.forward c s
    )

generatePawnAttacks :: UArray (Color, Square) BitBoard
generatePawnAttacks =
  makeArray
    ( \(c, s) ->
      maybe
        BitBoard.empty
        ( \square ->
            foldr
              ( (.|.)
                . maybe BitBoard.empty BitBoard.fromSquare
              )
              BitBoard.empty
                [ Square.left square
                , Square.right square]
        )
        $ Square.forward c s
    )
