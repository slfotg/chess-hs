{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.RanksFiles
  ( Squares (..)
  , edges
  , getAdjacentFiles) where

import           Chess.BitBoard     (BitBoard)
import qualified Chess.BitBoard     as BitBoard
import           Chess.File         (File (..))
import           Chess.Rank         (Rank (..))
import qualified Chess.Square       as Square
import           Data.Array.Unboxed (UArray, (!))
import           Data.Bits          ((.|.))
import           RIO                (Eq ((==)), Foldable (foldr),
                                     Num (abs, (-)))
import           Util.BoundedIndex  (makeArray, toIndex)

class Squares a where
  getSquares :: a -> BitBoard

{- |

>>> BitBoard.BBShow $ getSquares Second
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
X X X X X X X X
. . . . . . . .
-}
instance Squares Rank where
  getSquares = (!) ranks

{- |

>>> BitBoard.BBShow $ getSquares D
. . . X . . . .
. . . X . . . .
. . . X . . . .
. . . X . . . .
. . . X . . . .
. . . X . . . .
. . . X . . . .
. . . X . . . .
-}
instance Squares File where
  getSquares = (!) files

ranks :: UArray Rank BitBoard
ranks = makeArray (\r -> BitBoard.fromFilter (\s -> Square.getRank s == r))

files :: UArray File BitBoard
files = makeArray (\f -> BitBoard.fromFilter (\s -> Square.getFile s == f))

adjacentFiles :: UArray File BitBoard
adjacentFiles = makeArray (\f -> BitBoard.fromFilter (\s -> abs (toIndex (Square.getFile s) - toIndex f) == 1))


{- |

>>> BitBoard.BBShow $ getAdjacentFiles D
. . X . X . . .
. . X . X . . .
. . X . X . . .
. . X . X . . .
. . X . X . . .
. . X . X . . .
. . X . X . . .
. . X . X . . .

>>> BitBoard.BBShow $ getAdjacentFiles A
. X . . . . . .
. X . . . . . .
. X . . . . . .
. X . . . . . .
. X . . . . . .
. X . . . . . .
. X . . . . . .
. X . . . . . .

>>> BitBoard.BBShow $ getAdjacentFiles H
. . . . . . X .
. . . . . . X .
. . . . . . X .
. . . . . . X .
. . . . . . X .
. . . . . . X .
. . . . . . X .
. . . . . . X .
-}
getAdjacentFiles :: File -> BitBoard
getAdjacentFiles = (!) adjacentFiles

{- |

>>> BitBoard.BBShow edges
X X X X X X X X
X . . . . . . X
X . . . . . . X
X . . . . . . X
X . . . . . . X
X . . . . . . X
X . . . . . . X
X X X X X X X X
-}
edges :: BitBoard
edges = foldr (.|.) BitBoard.empty [getSquares A, getSquares H, getSquares First, getSquares Eighth]
