{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.Rays
  ( bishopRays
  , rookRays
  , getRookRays
  , getBishopRays
  , getQueenRays
  , getRays) where

import           Chess.BitBoard     (BitBoard, fromFilter)
import           Chess.Piece        (Piece (..))
import           Chess.Square       (Square)
import qualified Chess.Square       as Square
import           Data.Array.Unboxed (UArray, (!))
import           Data.Bits          (Bits (xor))
import           RIO                (Eq ((/=), (==)), Num (abs, (-)), ($), (&&),
                                     (||))
import           Util.BoundedIndex  (makeArray, toIndex)

bishopRays :: UArray Square BitBoard
bishopRays = makeArray bishopRays'
  where
    bishopRays' :: Square -> BitBoard
    bishopRays' source =
      fromFilter
        (\dest ->
          let sourceRank = toIndex $ Square.getRank source
              destRank   = toIndex $ Square.getRank dest
              sourceFile = toIndex $ Square.getFile source
              destFile   = toIndex $ Square.getFile dest
            in
              abs (sourceRank - destRank) == abs (sourceFile - destFile)
                && source /= dest
        )

rookRays :: UArray Square BitBoard
rookRays = makeArray rookRays'
  where
    rookRays' :: Square -> BitBoard
    rookRays' source =
      fromFilter
        (\dest ->
          let sourceRank = Square.getRank source
              destRank   = Square.getRank dest
              sourceFile = Square.getFile source
              destFile   = Square.getFile dest
            in
              (sourceRank == destRank || sourceFile == destFile)
                && source /= dest
        )

getRays :: Square -> Piece -> BitBoard
getRays s Rook = getRookRays s
getRays s _    = getBishopRays s

{- |

>>> getRookRays Square.C3
. . X . . . . .
. . X . . . . .
. . X . . . . .
. . X . . . . .
. . X . . . . .
X X . X X X X X
. . X . . . . .
. . X . . . . .
-}
getRookRays :: Square -> BitBoard
getRookRays = (!) rookRays

{- |

>>> getBishopRays Square.C3
. . . . . . . X
. . . . . . X .
. . . . . X . .
X . . . X . . .
. X . X . . . .
. . . . . . . .
. X . X . . . .
X . . . X . . .
-}
getBishopRays :: Square -> BitBoard
getBishopRays = (!) bishopRays


{- |

>>> getQueenRays Square.C3
. . X . . . . X
. . X . . . X .
. . X . . X . .
X . X . X . . .
. X X X . . . .
X X . X X X X X
. X X X . . . .
X . X . X . . .
-}
getQueenRays :: Square -> BitBoard
getQueenRays square = getBishopRays square `xor` getRookRays square
