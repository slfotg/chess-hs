{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.King
  ( kingMoves
  , kingsideCastles
  , queensideCastles
  , castleMoves
  ) where

import           Chess.BitBoard     (BitBoard)
import qualified Chess.BitBoard     as BitBoard
import           Chess.Color        (Color, toMyBackrank)
import qualified Chess.File         as File
import           Chess.Square       (Square (..))
import qualified Chess.Square       as Square
import           Data.Array.Unboxed (UArray, listArray, (!))
import           Data.Bits          (Bits (xor), (.|.))
import           RIO                (Bounded (maxBound, minBound),
                                     Eq ((/=), (==)), Num (abs, (-)), foldr,
                                     map, ($), (&&), (.), (||))
import           Util.BoundedIndex  (makeArray, toIndex)

{-|

__Examples__
>>> BitBoard.BBShow $ kingMoves D5
. . . . . . . .
. . . . . . . .
. . X X X . . .
. . X . X . . .
. . X X X . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
-}
kingMoves :: Square -> BitBoard
kingMoves = (!) genKingMoves

genKingMoves :: UArray Square BitBoard
genKingMoves = makeArray kingMoves'
  where
    kingMoves' :: Square -> BitBoard
    kingMoves' source =
      BitBoard.fromFilter
        (\dest ->
          let sourceRank = toIndex $ Square.getRank source
              destRank   = toIndex $ Square.getRank dest
              sourceFile = toIndex $ Square.getFile source
              destFile   = toIndex $ Square.getFile dest
            in
              (abs (sourceRank - destRank) == 1 || abs (sourceRank - destRank) == 0)
                && (abs (sourceFile - destFile) == 1 || abs (sourceFile - destFile) == 0)
                && source /= dest
        )

kingsideCastles :: UArray Color BitBoard
kingsideCastles =
  listArray
    (minBound, maxBound)
    $ map
      ( \c ->
        BitBoard.set (toMyBackrank c) File.F
          `xor` BitBoard.set (toMyBackrank c) File.G
      )
      [minBound, maxBound]

queensideCastles :: UArray Color BitBoard
queensideCastles =
  listArray
    (minBound, maxBound)
    $ map
      ( \c ->
        BitBoard.set (toMyBackrank c) File.B
          `xor` BitBoard.set (toMyBackrank c) File.C
          `xor` BitBoard.set (toMyBackrank c) File.D
      )
      [minBound, maxBound]

{- |

>>> BitBoard.BBShow castleMoves
. . X . X . X .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . X . X . X .
-}
castleMoves :: BitBoard
castleMoves =
  foldr
    ((.|.) . BitBoard.fromSquare)
    0
    [ C1
    , C8
    , E1
    , E8
    , G1
    , G8
    ]

