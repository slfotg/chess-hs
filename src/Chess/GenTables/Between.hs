{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.Between
  ( between
  ) where

import           Chess.BitBoard     (BitBoard, fromFilter)
import           Chess.Square       (Square, getFile, getRank)
import           Data.Array.Unboxed (UArray, (!))
import           RIO                (Bool, Eq ((/=), (==)), Num (abs, (-)),
                                     Ord ((<), (>)), otherwise, uncurry, ($),
                                     (&&), (||))
import           Util.BoundedIndex  (makeArray, toIndex)

isBetween :: Ord a => a -> a -> a -> Bool
isBetween a t b
  | a < b     = a < t && t < b
  | otherwise = a > t && t > b

isSquareBetween :: Square -> Square -> Square -> Bool
isSquareBetween source dest square =
  let sourceRank = toIndex $ getRank source
      destRank   = toIndex $ getRank dest
      testRank   = toIndex $ getRank square
      sourceFile = toIndex $ getFile source
      destFile   = toIndex $ getFile dest
      testFile   = toIndex $ getFile square
    in
      if abs (sourceRank - destRank) == abs (sourceFile - destFile)
        then
          abs (sourceRank - testRank) == abs (sourceFile - testFile)
            && abs (destRank - testRank) == abs (destFile - testFile)
            && isBetween sourceRank testRank destRank
        else
          ((sourceRank == destRank || sourceFile == destFile) && source /= dest) && (
          (sourceRank == testRank
            && destRank == testRank
            && isBetween sourceFile testFile destFile)
          ||
          (sourceFile == testFile
            && destFile == testFile
            && isBetween sourceRank testRank destRank))

between' :: UArray (Square, Square) BitBoard
between' = makeArray $ uncurry createBitBoard
  where
    createBitBoard :: Square -> Square -> BitBoard
    createBitBoard source dest = fromFilter (isSquareBetween source dest)

between :: (Square, Square) -> BitBoard
between = (!) between'
