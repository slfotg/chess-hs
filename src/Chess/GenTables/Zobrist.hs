{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.Zobrist
  ( hashPiece
  , hashCastles
  , hashEnPassant
  , hashColor
  ) where

import           Chess.CastleRights (CastleRights)
import           Chess.Color        (Color (..))
import           Chess.File         (File)
import           Chess.Piece        (Piece)
import           Chess.Square       (Square)
import           Data.Array.Unboxed (UArray, listArray, (!))
import           Data.Bits          (Bits (..))
import           RIO                (Bounded (maxBound, minBound), Int,
                                     Num ((+), (-)), Show, Word64, drop, map,
                                     ($))
import           RIO.List           (iterate, splitAt)
import           RIO.List.Partial   (head)

{- | Random Number Generator using KISS

>>> let (x, _) = splitAt 10 $ rKiss 42 in x
ProgressCancelledException
-}
rKiss :: Word64 -> [Word64]
rKiss seed =
  drop 20
    $ map last4
      $ iterate nextValue (0xf1ea5eed, seed, seed, seed)
  where
    nextValue :: (Word64, Word64, Word64, Word64) -> (Word64, Word64, Word64, Word64)
    nextValue (a, b, c, d) =
      let e = a - rot b 7
        in
          ( b `xor` rot c 13
          , c + rot d 37
          , d + e
          , a + e
          )

    last4 :: (a, b, c, d) -> d
    last4 (_, _, _, d) = d

    rot :: Word64 -> Int -> Word64
    rot x k = x `shiftL` k .|. x `shiftR` (64 - k)

data Zobrist = Zobrist
  { _piece      :: !(UArray (Color, Piece, Square) Word64)
  , _sideToMove :: !Word64
  , _castle     :: !(UArray (Color, CastleRights) Word64)
  , _enPassant  :: !(UArray File Word64)
  }
  deriving (Show)

zobrist :: Zobrist
zobrist =
  let (piece, notPiece) = splitAt 768 $ rKiss 42
      (side, notSide) = splitAt 1 notPiece
      (castle, notCastle) = splitAt 8 notSide
      (ep, _) = splitAt 8 notCastle
    in
      Zobrist
        { _piece      = listArray (minBound, maxBound) piece
        , _sideToMove = head side
        , _castle     = listArray (minBound, maxBound) castle
        , _enPassant  = listArray (minBound, maxBound) ep
        }

hashPiece :: Piece -> Square -> Color -> Word64
hashPiece p s c = _piece zobrist ! (c, p, s)

hashCastles :: CastleRights -> Color -> Word64
hashCastles cr c = _castle zobrist ! (c, cr)

hashEnPassant :: File -> Word64
hashEnPassant f = _enPassant zobrist ! f

hashColor :: Color -> Word64
hashColor White = 0
hashColor Black = _sideToMove zobrist
