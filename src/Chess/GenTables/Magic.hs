{-# LANGUAGE NoImplicitPrelude #-}
module Chess.GenTables.Magic
  ( rookBits
  , bishopBits
  , numMoves
  , rookDirections
  , bishopDirections
  , magicMask
  , raysToQuestions
  ) where

import           Chess.BitBoard             (BitBoard)
import qualified Chess.BitBoard             as BitBoard
import           Chess.GenTables.RanksFiles (edges)
import           Chess.GenTables.Rays       (getRays)
import           Chess.Piece                (Piece (..))
import           Chess.Square               (Square (..))
import qualified Chess.Square               as Square
import           Data.Bits                  (Bits (..))
import           RIO                        (Int, Maybe (..), Num ((*), (+)),
                                             Vector, map, maybe, ($), (.))
import           RIO.List                   (subsequences)
import           RIO.Vector                 (fromList)

-- | How many squares can a blocking piece be on for the rook?
rookBits :: Int
rookBits = 12

-- | How many squares can a blocking piece be on for a bishop?
bishopBits :: Int
bishopBits = 9

-- | How many different sets of moves for both rooks and bishops are there?
numMoves :: Int
numMoves = 64 * (1 `shiftL` rookBits) + 64 * (1 `shiftL` bishopBits)

{- | Return a list of directions for the rook.

__Examples__
>>> fmap ($ Square.C6) $ toList rookDirections
[Just b6,Just d6,Just c7,Just c5]

>>> fmap ($ Square.A6) $ toList rookDirections
[Nothing,Just b6,Just a7,Just a5]

>>> fmap ($ Square.H1) $ toList rookDirections
[Just g1,Nothing,Just h2,Nothing]
-}
rookDirections :: Vector (Square -> Maybe Square)
rookDirections =
  fromList
    [ Square.left
    , Square.right
    , Square.up
    , Square.down
    ]

{- | Return a list of directions for the bishop.

__Examples__
>>> fmap ($ Square.C6) $ toList bishopDirections
[Just b7,Just d7,Just b5,Just d5]

>>> fmap ($ Square.A6) $ toList bishopDirections
[Nothing,Just b7,Nothing,Just b5]

>>> fmap ($ Square.H1) $ toList bishopDirections
[Just g2,Nothing,Nothing,Nothing]
-}
bishopDirections :: Vector (Square -> Maybe Square)
bishopDirections =
  fromList
    [ maybe Nothing Square.up   . Square.left
    , maybe Nothing Square.up   . Square.right
    , maybe Nothing Square.down . Square.left
    , maybe Nothing Square.down . Square.right
    ]

-- idk about this one, i hope it's not used anywhere
-- randomBitBoard :: BitBoard

{- | Given a square and the type of piece, lookup the RAYS and remove the endpoint squares.

__Examples__
>>> BitBoard.BBShow (magicMask D4 Bishop)
. . . . . . . .
. . . . . . X .
. X . . . X . .
. . X . X . . .
. . . . . . . .
. . X . X . . .
. X . . . X . .
. . . . . . . .

>>> BitBoard.BBShow (magicMask D4 Rook)
. . . . . . . .
. . . X . . . .
. . . X . . . .
. . . X . . . .
. X X . X X X .
. . . X . . . .
. . . X . . . .
. . . . . . . .
-}
magicMask :: Square -> Piece -> BitBoard
magicMask square piece =
  getRays square piece
  .&.
  complement edges

{- | Given a bitboard, generate a list of every possible set of bitboards using those bits.
AKA, if 'n' bits are set, generate 2^n bitboards where b1|b2|b3|...b(2^n) == mask

>>> RIO.fmap BitBoard.BBShow $ raysToQuestions $ BitBoard.fromSquares [A1, C4, F2]
[
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
,
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
X . . . . . . .
,
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . X . .
. . . . . . . .
,
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . X . .
X . . . . . . .
,
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . X . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
,
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . X . . . . .
. . . . . . . .
. . . . . . . .
X . . . . . . .
,
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . X . . . . .
. . . . . . . .
. . . . . X . .
. . . . . . . .
,
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . X . . . . .
. . . . . . . .
. . . . . X . .
X . . . . . . .
]
  -}
raysToQuestions :: BitBoard -> Vector BitBoard
raysToQuestions bb =
  fromList
    $ map
      BitBoard.fromSquares
      $ subsequences
        $ BitBoard.allSquares bb
