{-# LANGUAGE NoImplicitPrelude #-}
module Chess.BitBoard
  ( BitBoard
  , BBShow (..)
  , showBitBoard
  , set
  , fromSquare
  , fromSquares
  , fromFilter
  , fromMaybeSquare
  , empty
  , reverseColors
  , allSquares
  , toSize
  , toSquare
  ) where

import           Chess.File        (File)
import           Chess.Rank        (Rank)
import           Chess.Square      (Square (..), makeSquare)
import           Data.Bits         (Bits (..), FiniteBits (countTrailingZeros))
import           RIO               (Bool, Foldable, Int, Maybe, Show (..),
                                    String, Word64, byteSwap64, concatMap,
                                    filter, fmap, foldr, fromIntegral, map,
                                    reverse, take, ($), (+), (.))
import           RIO.List          (intercalate, iterate)
import           Util.BoundedIndex (fromIndex, iter, toIndex)

type BitBoard = Word64

newtype BBShow = BBShow BitBoard

instance Show BBShow where
  show (BBShow w) = showBitBoard w

showBitBoard :: BitBoard -> String
showBitBoard w =
  intercalate "\n"
    . map
    ( concatMap
      (\x ->
        if testBit w x
          then "X "
          else ". "
      )
    )
    . reverse
    . take 8
    $ iterate (map (+8)) [0..7]

empty :: BitBoard
empty = 0

-- | Construct a new `BitBoard` with a particular `Square` set
set :: Rank -> File -> BitBoard
set rank file = fromSquare $ makeSquare rank file

{- | Construct a new `BitBoard` with a particular `Square` set

>>> (fromSquare A2) .|. (fromSquare D1)
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
X . . . . . . .
. . . X . . . .
-}
fromSquare :: Square -> BitBoard
fromSquare = shiftL 1 . toIndex

fromSquares :: Foldable t => t Square -> BitBoard
fromSquares = foldMap fromSquare

foldMap :: Foldable t => (a -> BitBoard) -> t a -> BitBoard
foldMap f = foldr ((.|.) . f) 0

fromFilter :: (Square -> Bool) -> BitBoard
fromFilter f =
  foldMap
    fromSquare
    $ filter f iter

-- | Convert a `Maybe Square` to a `Maybe BitBoard`
fromMaybeSquare :: Maybe Square -> Maybe BitBoard
fromMaybeSquare = fmap fromSquare

{- | Convert a `BitBoard` to a `Square`.  This grabs the least-significant `Square`

>>> toSquare $ fromSquare A2
a2

>>> toSquare empty
a1
-}
toSquare :: BitBoard -> Square
toSquare w = fromIndex $ countTrailingZeros w

{-
>>> reverseColors $ (fromSquare A2) .|. (fromSquare D1)
. . . X . . . .
X . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . . .
-}
reverseColors :: BitBoard -> BitBoard
reverseColors = byteSwap64

toSize :: BitBoard -> Int -> Int
toSize w = fromIntegral . shiftR w

{- |
>>> allSquares $ (fromSquare A2) .|. (fromSquare D1)
[d1,a2]
-}
allSquares :: BitBoard -> [Square]
allSquares 0 = []
allSquares b = let square = toSquare b
  in
    square : allSquares (b `xor` fromSquare square)
