{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Chess.Piece
  ( Piece (..)
  , numPieces
  , numPromotionPieces
  , promotionPieces
  , toString
  ) where

import           Chess.Color       (Color (Black, White))
import           Data.Ix           (Ix)
import           RIO               (Bounded, Either (..), Enum (..), Eq, Int,
                                    Maybe (Just), Ord, Show (..), String,
                                    Vector, map, ($))
import           RIO.Char          (toUpper)
import           RIO.Text          (uncons)
import           RIO.Vector.Boxed  (fromList)
import           Util.BoundedIndex (BoundedIndex)
import           Util.Error        (Error (..))
import           Util.FromText     (FromChar (fromChar), FromText (fromText))

-- | Represent a chess piece as a very simple enum
data Piece = Pawn
           | Knight
           | Bishop
           | Rook
           | Queen
           | King
  deriving (Bounded, Eq, Enum, Ix, Ord)

instance BoundedIndex Piece

-- | How many piece types are there?
numPieces :: Int
numPieces = 6

-- | How many ways can I promote?
numPromotionPieces :: Int
numPromotionPieces = 4

-- | What pieces can I promote to?
promotionPieces :: Vector Piece
promotionPieces =
  fromList
    [ Queen
    , Knight
    , Rook
    , Bishop
    ]

{- | Convert a piece with a color to a string.
White pieces are uppercase, black pieces are lowercase.

__Examples:__

> toString King White == "K"
> toString Knight Black == "n"

-}
toString :: Piece -> Color -> String
toString p White = map toUpper $ show p
toString p Black = show p

instance FromText Piece where
  fromText text =
    case uncons text of
      Just (c, "") -> fromChar c
      _            -> Left InvalidPiece

instance FromChar Piece where
  fromChar 'p' = Right Pawn
  fromChar 'n' = Right Knight
  fromChar 'b' = Right Bishop
  fromChar 'r' = Right Rook
  fromChar 'q' = Right Queen
  fromChar 'k' = Right King
  fromChar _   = Left InvalidPiece

instance Show Piece where
  show Pawn   = "p"
  show Knight = "n"
  show Bishop = "b"
  show Rook   = "r"
  show Queen  = "q"
  show King   = "k"
