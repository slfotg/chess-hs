{-# LANGUAGE NoImplicitPrelude #-}
module Chess.Color
  ( Color (..)
  , numColors
  , toMyBackrank
  , toTheirBackrank
  , toSecondRank
  , toFourthRank
  , toSeventhRank
  , other
  ) where

import           Chess.Rank        (Rank (..))
import           Data.Ix           (Ix)
import           RIO               (Bounded, Enum, Eq, Int, Ord, Show)
import           Util.BoundedIndex (BoundedIndex)

-- | Represent a color.
data Color = White
           | Black
  deriving (Bounded, Eq, Enum, Ix, Ord, Show)

instance BoundedIndex Color

-- | How many colors are there?
numColors :: Int
numColors = 2

-- | Convert a `Color` to my backrank, which represents the starting rank for my pieces.
toMyBackrank :: Color -> Rank
toMyBackrank White = First
toMyBackrank Black = Eighth
{-# INLINE toMyBackrank #-}

-- | Convert a `Color` to my opponents backrank, which represents the starting rank for the opponents pieces.
toTheirBackrank :: Color -> Rank
toTheirBackrank White = Eighth
toTheirBackrank Black = First
{-# INLINE toTheirBackrank #-}

-- | Convert a `Color` to my second rank, which represents the starting rank for my pawns.
toSecondRank :: Color -> Rank
toSecondRank White = Second
toSecondRank Black = Seventh
{-# INLINE toSecondRank #-}

-- | Convert a `Color` to my fourth rank, which represents the rank of my pawns when moving two squares forward.
toFourthRank :: Color -> Rank
toFourthRank White = Fourth
toFourthRank Black = Fifth
{-# INLINE toFourthRank #-}

-- | Convert a `Color` to my seventh rank, which represents the rank before pawn promotion.
toSeventhRank :: Color -> Rank
toSeventhRank White = Seventh
toSeventhRank Black = Second
{-# INLINE toSeventhRank #-}

-- | Get the other color.
other :: Color -> Color
other White = Black
other Black = White
{-# INLINE other #-}
