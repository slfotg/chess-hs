{-# LANGUAGE NoImplicitPrelude #-}
module Chess.ChessMove
  ( ChessMove (..)
  , fromSan
  ) where

import           Chess.Board   (Board)
import           Chess.Piece   (Piece (..))
import           Chess.Square  (Square)
import           RIO           (Applicative ((<*>)), Bifunctor (first),
                                Either (Left, Right), Eq, Maybe (..), Ord,
                                Show (..), String, catMaybes, concat, const,
                                elem, undefined, ($), (<$>))
import qualified RIO.Text      as Text
import           Util.Error    (Error (InvalidUciMove), Result)
import           Util.FromText (FromText (..))

data ChessMove =
  ChessMove
    { source    :: Square
    , dest      :: Square
    , promotion :: Maybe Piece
    }
  deriving (Eq, Ord)

{- | Convert a SAN (Standard Algebraic Notation) move into a `ChessMove`

>>> fromSan defaultBoard "e4"
-}
fromSan :: Board -> String -> Result ChessMove
fromSan = undefined

instance Show ChessMove where
  show cm =
    concat
      $ catMaybes
        [ Just $ show $ source cm
        , Just $ show $ dest cm
        , show <$> promotion cm
        ]

{- | Convert a UCI `Text` to a move. If invalid, return `Error`

>>> fromText $ Text.pack "e7e8q" :: Result ChessMove
NOW Right e7e8q
-}
instance FromText ChessMove where
  fromText str =
    let moveConstructor s d = ChessMove <$> fromText s <*> fromText d
    in
    first
      (const InvalidUciMove)
      $ case Text.chunksOf 2 str of
        [s, d, ep] ->
          moveConstructor s d
            <*> case fromText ep of
              Right piece  ->
                if piece `elem` [Pawn, King]
                  then Left InvalidUciMove
                  else Right $ Just piece
              _            -> Left InvalidUciMove
        [s, d]   ->
          moveConstructor s d
            <*> Right Nothing
        _           -> Left InvalidUciMove

