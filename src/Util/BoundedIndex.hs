{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Util.BoundedIndex
  ( BoundedIndex (..)
  , makeArray
  ) where

import           Data.Array.IArray  (IArray)
import           Data.Array.Unboxed (Ix (..), UArray, listArray)
import           RIO                (Bounded (..), Int, Vector, map, ($))
import           RIO.Vector         (fromList)
import           RIO.Vector.Partial ((!))

class (Bounded a, Ix a) => BoundedIndex a where
  toIndex :: a -> Int
  toIndex = index (minBound, maxBound)

  fromIndex :: Int -> a
  fromIndex = (!) allElements

  iter :: [a]
  iter = range (minBound, maxBound)

  allElements :: Vector a
  allElements = fromList iter

instance (BoundedIndex a, BoundedIndex b) => BoundedIndex (a, b)

-- TODO I don't think this belongs here
{- | Create an array from a function that maps a bounded index to the element
-}
makeArray :: (BoundedIndex i, IArray UArray e) => (i -> e) -> UArray i e
makeArray f =
  listArray
    (minBound, maxBound)
    $ map f iter

