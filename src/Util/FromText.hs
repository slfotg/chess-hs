{-# LANGUAGE NoImplicitPrelude #-}
module Util.FromText
  ( FromText (..)
  , FromChar (..)
  ) where

import           RIO        (Char, Text)
import           Util.Error (Result)

class FromText a where
  fromText :: Text -> Result a

class FromChar a where
  fromChar :: Char -> Result a
