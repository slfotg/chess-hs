{-# LANGUAGE NoImplicitPrelude #-}
module Util.Error
  ( Error (..)
  , Result
  ) where

import           RIO      (Either, Show (..), Text, (++))
import           RIO.Text (unpack)

data Error
  = InvalidFen !Text   -- ^ The FEN string is invalid
  | InvalidBoard       -- ^ The board created from BoardBuilder was found to be invalid
  | InvalidSquare      -- ^ An attempt was made to create a square from an invalid string
  | InvalidSanMove     -- ^ An attempt was made to create a move from an invalid SAN string
  | InvalidUciMove     -- ^ An atempt was made to create a move from an invalid UCI string
  | InvalidRank        -- ^ An attempt was made to convert a string not equal to "1"-"8" to a rank
  | InvalidFile        -- ^ An attempt was made to convert a string not equal to "a"-"h" to a file
  | InvalidPiece
  | InvalidEnPassant !Text

instance Show Error where
  show (InvalidFen fen)      = "Invalid FEN string: " ++ unpack fen
  show InvalidBoard          = "The board specified did not pass sanity checks.  Are you sure the kings exist and the side to move cannot capture the opposing king?"
  show InvalidSquare         = "The string specified does not contain a valid algebraic notation square"
  show InvalidSanMove        = "The string specified does not contain a valid SAN notation move"
  show InvalidUciMove        = "The string specified does not contain a valid UCI notation move"
  show InvalidRank           = "The string specified does not contain a valid rank"
  show InvalidFile           = "The string specified does not contain a valid file"
  show InvalidPiece          = "The string specified does not contain a valid piece"
  show (InvalidEnPassant ep) = "The string specified is not a valid en passant square: " ++ unpack ep

type Result a = Either Error a
